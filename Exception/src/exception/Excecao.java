package exception;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Ana
 */
public class Excecao extends Application {
    private static Stage stage;
    public static MenuController controllerMenu;
    ArrayList<Animal> animais = new ArrayList();
    
    public static Stage getStage() {
        return stage;
    }
    @Override
    public void start(Stage stage) throws Exception {
               
        Parent root = null;
        FXMLLoader loader = new FXMLLoader(Excecao.class.getResource("Menu.fxml"));
        root = loader.load();
        Scene scene = new Scene(root);
             
        controllerMenu = (MenuController)loader.getController();
        controllerMenu.primeiro(animais);
        
        Excecao.stage = stage;
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
