package exception;

/**
 *
 * @author Ana
 */
public class Data {
    private int dia, mes, ano;
    String[] dataS;
    
    public Data(String data) throws DiaException, MesException, AnoException{
        dataS = data.split("/");
        
        setDia(Integer.parseInt(dataS[0]));
        setMes(Integer.parseInt(dataS[1]));
        setAno(Integer.parseInt(dataS[2]));
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) throws DiaException{
        if (dia>0 && dia<32){
            this.dia = dia;
        }
        else{
            throw new DiaException();
        }
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) throws MesException {
        if (mes>0 && mes<13){
            this.mes = mes;
        }
        else{
            throw new MesException();
        }
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) throws AnoException{
        if (ano>1){
            this.ano = ano;
        }
        else{
            throw new AnoException();
        }
    }
    @Override
    public String toString(){
        return dataS[0]+"/"+dataS[1]+"/"+ dataS[2];
    }
    
}
