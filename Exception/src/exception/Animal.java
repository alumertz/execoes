package exception;

/**
 *
 * @author Ana
 */
public abstract class Animal {
    double peso;
    String nome;
    String raca;
    Data nascimento;
    
    public Animal (String nome, double peso, String raca, Data data){
        this.nome =nome;
        this.peso = peso;
        this.raca = raca;
        this.nascimento = data;
    }
    
}
