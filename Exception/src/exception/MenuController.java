package exception;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * MenuController
 *
 * @author Ana
 */
public class MenuController implements Initializable {
    
    @FXML
    Label aviso;
    
    @FXML
    TextField peso, raca, nome,data;
    
    @FXML
    TextArea relatorio;
    
    @FXML
    private ChoiceBox escolher = new ChoiceBox();

    ArrayList<String> list = new ArrayList<>();
    ObservableList<String>oList = FXCollections.observableList(list);
    ArrayList<Animal> animais;
    
    public void primeiro(ArrayList<Animal> vetAnimal){
        animais = vetAnimal;
    }
    
    @FXML
    public void adicionarAnimal(){
        System.out.println("animais size: "+ animais.size());
        String opcaoEscolhida = escolher.getSelectionModel().getSelectedItem().toString();
        double vPeso = Double.parseDouble(peso.getText());
        String vNome= nome.getText(), vRaca = raca.getText(), vData = data.getText();
        try {
            Data dataN = new Data(vData);
            switch(opcaoEscolhida){
            case "Cachorro": animais.add(new Cachorro(vNome, vPeso, vRaca,dataN));
            break;
            case "Gato": animais.add(new Gato(vNome, vPeso, vRaca,dataN));
            break;
            case "Passaro": animais.add(new Passaro(vNome, vPeso, vRaca,dataN));
            break;
        }
        }
        catch(DiaException diaE){
            aviso.setText(diaE.getMessage());
        }
        catch(MesException mesE){
            aviso.setText(mesE.getMessage());
        }
        catch (AnoException anoE){
            aviso.setText(anoE.getMessage());
        }
        nome.setText("");
        peso.setText("");
        raca.setText("");
        data.setText("");
        
        System.out.println("animais size: "+ animais.size());
    }
    
    @FXML
    public void listar(){
        String tudo="";
        for (int x=0; x<animais.size(); x++){
            tudo = tudo + animais.get(x).toString();
        }
        relatorio.setText(tudo);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        list.add("Cachorro");
        list.add("Gato");
        list.add("Passaro");
        escolher.setItems(oList);
    }    
    
}
