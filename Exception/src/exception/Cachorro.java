package exception;

/**
 *
 * @author Casa
 */
public class Cachorro extends Animal{
    public Cachorro(String nome, double peso, String raca, Data data){
        super (nome, peso, raca, data);
    }
    @Override
    public String toString(){
        return "Nome: "+nome+" - Cachorro\nRaça: "+ raca+"  Peso: "+ peso +"  D.Nasc.: "+nascimento.toString()+"\n\n";
    }
}
