package exception;

/**
 *
 * @author Ana
 */
public class Passaro extends Animal{
    public Passaro(String nome, double peso, String raca, Data data){
        super (nome, peso, raca, data);
    }
    @Override
    public String toString(){
        return "Nome: "+nome+" - Passaro\nRaça: "+ raca+" Peso: "+ peso +" D.Nasc.: "+nascimento.toString()+"\n\n";
    }
}
