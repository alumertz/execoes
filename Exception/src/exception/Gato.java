package exception;

/**
 *
 * @author Ana
 */
public class Gato extends Animal{
    public Gato(String nome, double peso, String raca, Data data){
        super (nome, peso, raca, data);
    }
    @Override
    public String toString(){
        return "Nome: "+nome+" - Gato\nRaça: "+ raca +" Peso: "+ peso +" D.Nasc.: "+nascimento.toString()+"\n\n";
    }
}
